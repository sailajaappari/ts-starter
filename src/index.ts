import { List, Range, Seq } from 'immutable';


export class ImmutableCounter {

counter: number;

constructor(initVal: number) {
this.counter = initVal;
}

getValue(): number {
return this.counter;
}

Incriment(): any {
return new ImmutableCounter(this.counter + 1);
}

Decrement(): any {

return new ImmutableCounter(this.counter - 1) ;
}
}

// Prime Numbers
export const isPrime = (n: number) :boolean => {
    return Range(2, n).every(x => n % x !== 0);
// if (Range(2, n / 2 ).filter(x => n % x === 0).equals([])) {
// return true;
// }
// else
// {
// return false;
// }

}

export const Primes = (nums: number) =>
nums.filter(isPrime); 

// Pascal Triangle
export const Factorial = (n: number) :number =>
Range(1, n + 1).reduce((x, y) => x * y, 1);

export const NCR = (n: number, r: number) =>
Factorial(n)/(Factorial (r) * Factorial(n-r))



export const PascalTriangle = (num :number) => {
  Range(1, num + 1).map(n => { Range(1, n+1).map(r => NCR(n,r))})
}




