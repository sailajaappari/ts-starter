
import { ImmutableCounter, Primes, isPrime, Factorial, NCR, PascalTriangle } from './index';
import { List, Range } from 'immutable';

test('Increment test', () => {
let a = new ImmutableCounter(3);
let b = a.Incriment();
expect(a.getValue() + 1).toBe(b.getValue());
});

test('Decrement test', () => {
let a = new ImmutableCounter(3);
let b = a.Decrement();
expect(a.getValue() - 1).toBe(b.getValue());
});

test('Objects Not Equals', () => {
let a = new ImmutableCounter(5);
let b = a.Decrement();
expect(a===b).toBeFalsy();
})

test('prime number', () => {
//const nums = List(Range(2, 10));
expect(isPrime(4)).toBeFalsy();
});


test('prime numbers', () => {
let nums = List(Range(2, 10));
expect(Primes(nums)).toEqual(List([2, 3, 5, 7]));
});

test('factorial', () => {
expect(Factorial(5)).toBe(120);
})

test('factorial -1', () => {
expect(Factorial(-1)).toBe(1);
})

test('factorial0', () => {
expect(Factorial(0)).toBe(1);
})

test('ncr', () => {
expect(NCR(0, 0)).toBe(1);
})

test('ncr', () => {
expect(NCR(2, 1)).toBe(2);
})


test('ncr', () => {
expect(NCR(2, -1)).toBe(2/6);
})

test('pascal triangle', () => {
expect(PascalTriangle(3)).toBe(List[[1], [1, 1], [1, 2, 1]]);
})